<?php


namespace Yeltrik\Color\app;

/**
 * Class Colors
 * @package Yeltrik\Color\app
 */
class Colors
{

    private array $keys;

    /**
     * Colors constructor.
     * @param array $keys
     */
    private function __construct(array $keys)
    {
        $this->keys = $keys;
    }

    /**
     * @param string $key
     * @return float|int
     */
    private function crc32Percentage(string $key)
    {
        return (crc32($key) / 2147483647);
    }

    /**
     * @param string $key
     * @param int $min
     * @param int $max
     * @return float
     */
    private function crc32Value(string $key, string $seed, int $min, int $max): float
    {
        return crc32($key . $seed) % ($max - $min) + $min;
    }

    /**
     * @param array $array
     * @return static
     */
    public static function fromArrayByKeys(array $array)
    {
        return new static(array_keys($array));
    }

    /**
     * @return array
     */
    public function generateRGBAColors(): array
    {
        $rgbaColors = [];
        foreach ($this->keys() as $key) {
            $rgbaColors[] = $this->generateRGBAFromKey($key);
        }
        return $rgbaColors;
    }

    /**
     * @param string $key
     * @return string
     */
    private function generateRGBAFromKey(string $key): string
    {
        $opacity = $this->generateOpacity($key, 2, 9);
        $r = intval($this->crc32Value($key, "R", 0, 255));
        $g = intval($this->crc32Value($key, "G", 0, 255));
        $b = intval($this->crc32Value($key, "B", 0, 255));
        return "rgba($r, $g, $b, $opacity)";
    }

    /**
     * @return float
     */
    private function generateOpacity(string $key, float $min = 0, float $max = 1): float
    {
        return $this->crc32Value($key, "O", $min, $max) / 10;
    }

    /**
     * @return false|string
     */
    public function jsonEncodedRGBAColors()
    {
        return json_encode($this->generateRGBAColors());
    }

    /**
     * @return array
     */
    private function keys(): array
    {
        return $this->keys;
    }

}
